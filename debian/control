Source: ndcube
Section: python
Priority: optional
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Prat <vivi@debian.org>
Build-Depends: debhelper (>= 13),
               debhelper-compat (= 13),
               dh-python,
               graphviz,
               python3-all,
               python3-astropy,
               python3-astropy-helpers,
               python3-gwcs,
               python3-parfive,
               python3-pytest,
               python3-reproject,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx (>= 2),
               python3-sphinx-astropy,
               python3-sphinx-automodapi,
               python3-sunpy,
               python3-tk
Standards-Version: 4.6.0.1
Homepage: https://docs.sunpy.org/projects/ndcube
Vcs-Git: https://salsa.debian.org/debian-astro-team/ndcube.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/ndcube/
Rules-Requires-Root: no

Package: python3-ndcube
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Suggests: python3-ndcube-doc
Description: Package for multi-dimensional coordinate-aware arrays (Python 3)
 ndcube is a SunPy affiliated package for manipulating, inspecting and
 visualizing multi-dimensional contiguous and non-contiguous coordinate-aware
 data arrays. It combines data, uncertainties, units, metadata, masking, and
 coordinate transformations into classes with unified slicing and generic
 coordinate transformations and plotting/animation capabilities. It is
 designed to handle data of any number of dimensions and axis types (e.g.
 spatial, temporal, spectral, etc.) whose relationship between the array
 elements and the real world can be described by World Coordinate System (WCS)
 translations.
 .
 This is the Python 3 version of the package.

Package: python3-ndcube-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${sphinxdoc:Depends},
         ${misc:Depends}
Description: Package for multi-dimensional coordinate-aware arrays (documentation)
 ndcube is a SunPy affiliated package for manipulating, inspecting and
 visualizing multi-dimensional contiguous and non-contiguous coordinate-aware
 data arrays. It combines data, uncertainties, units, metadata, masking, and
 coordinate transformations into classes with unified slicing and generic
 coordinate transformations and plotting/animation capabilities. It is
 designed to handle data of any number of dimensions and axis types (e.g.
 spatial, temporal, spectral, etc.) whose relationship between the array
 elements and the real world can be described by World Coordinate System (WCS)
 translations.
 .
 This is the common documentation package.
